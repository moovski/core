export const TYPE = {
  MOOVSKI: 1,
  STANDARD: 2
};

export interface Type {
  id: number;
  name: string;
  days: number;
}

export const TYPES: Type[] = [
  { id: TYPE.MOOVSKI, name: "Custom delivery", days: 0 },
  { id: TYPE.STANDARD, name: "Standard delivery", days: 0 }
];
