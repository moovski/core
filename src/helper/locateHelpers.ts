import * as _ from 'lodash';

const getDevices = (collection: any, location: any, where: {}): Promise<any[]> => {
    return new Promise<any>((resolve: any, reject: any) => {

        collection.aggregate([
        {
            $geoNear: {
                near: location,
                distanceField: 'jobDistance',
                spherical: true,
                query: where,
                distanceMultiplier: 0.001
            }
       },
       { '$redact': {
            '$cond': {
                'if': { '$lt': [ '$jobDistance', '$distance' ] },
                'then': '$$KEEP',
                'else': '$$PRUNE'
            }
        }},
        { $project: { _id: 0, devices: '$devices.pushId' } },
        { $unwind: '$devices' },
        { $unwind: '$devices' },
        { $group: { _id:'devices', results: { $addToSet:'$devices' } } },
        { $project: { _id: 0, results: 1 } }
        ], (err: Error, data: any) => {
            if (err) return reject(err);
            return resolve(_.get(data, '[0].results'));
        });
    });
};

const combinedResults = (helpers: any[]): any[] => {
    return _.union(helpers[0], helpers[1]);
};

export default (context: any): Promise<any[]> => {
    const job = context.job;
    context.whereClause = { notification: true };
    context.coordinatesFrom = job.locFrom;
    context.coordinatesTo = job.locTo;

    if (context.insured === true) {
        context.whereClause.insured = true;
    }

    const fromDevices = getDevices(
        context.db.collection('helper'),
        job.locFrom,
        context.whereClause
    );
    const toDevices = getDevices(
        context.db.collection('helper'),
        job.locTo,
        context.whereClause
    );
    return Promise.all([fromDevices, toDevices])
        .then(combinedResults);
};
