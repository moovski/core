export const STATUS = {
  QUOTE: 1,
  BOOKED: 2,
  CANCELLED: 3,
  ACCEPTED: 4,
  COMPLETED: 5,
  REFUNDED: 6,
  PAID: 7
};

export interface Status {
  value: number;
  text: string;
}

export let STATUSES: Status[] = [
  { value: STATUS.QUOTE, text: "Quote" },
  { value: STATUS.BOOKED, text: "Booked" },
  { value: STATUS.CANCELLED, text: "Cancelled" },
  { value: STATUS.ACCEPTED, text: "Accepted" },
  { value: STATUS.COMPLETED, text: "Completed" },
  { value: STATUS.REFUNDED, text: "Refunded" },
  { value: STATUS.PAID, text: "Paid" }
];
