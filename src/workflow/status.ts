export const STATUS = {
  DRAFT: 1,
  ACTIVE: 2,
  DISABLED: 3
};

export interface Status {
  value: number;
  text: string;
}

export let STATUSES: Status[] = [
  { value: STATUS.DRAFT, text: "Draft" },
  { value: STATUS.ACTIVE, text: "Active" },
  { value: STATUS.DISABLED, text: "Disabled" }
];

export interface ReferenceSource {
  getAll (): Status[];
}

export let SOURCE: ReferenceSource = {
  getAll (): Status[] {
    return STATUSES;
  }
};
