import { EntityDescriptor } from "./entityDescriptor";
export declare class Entity {
    descriptor: EntityDescriptor;
    constructor(descriptor: EntityDescriptor, values?: any);
    serialize(): any;
    getField(name: string): any;
    getRules(name: string): any;
}
export declare const createEntity: (descriptor: EntityDescriptor, data: any) => Entity;
