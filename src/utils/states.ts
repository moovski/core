import {ReferenceSource} from "../helper/status";

export interface State {
  value: number;
  text: string;
}

export const STATES: State[] = [
  { text: "New South Wales", value: 1 },
  { text: "Queensland", value: 2 },
  { text: "South Australia", value: 3 },
  { text: "Tasmania", value: 4 },
  { text: "Victoria", value: 5 },
  { text: "Western Australia", value: 6 }
];

export const STATE_SOURCE: ReferenceSource = {
  getAll (): State[] {
    return STATES;
  }
};
