export declare const STATUS: {
    DRAFT: number;
    ACTIVE: number;
    DISABLED: number;
};
export interface Status {
    value: number;
    text: string;
}
export declare let STATUSES: Status[];
export interface ReferenceSource {
    getAll(): Status[];
}
export declare let SOURCE: ReferenceSource;
