export declare const MONTHS: String[];
export interface IMapMonths {
    text: String;
    value: number;
}
export interface ReferenceMonthSource {
    getAll(): IMapMonths[];
}
export declare const MONTHS_SOURCE: ReferenceMonthSource;
export declare const DATE: any;
