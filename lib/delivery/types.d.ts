export declare const TYPE: {
    MOOVSKI: number;
    STANDARD: number;
};
export interface Type {
    id: number;
    name: string;
    days: number;
}
export declare const TYPES: Type[];
