export interface IMapGeneric {
    text: String;
    value: number;
}
export declare const years: (min?: number) => IMapGeneric[];
export declare const futureYears: (max?: number) => IMapGeneric[];
export declare const days: () => IMapGeneric[];
