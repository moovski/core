"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var fields = require("../entities/fields");
var STATES_MODULE = require("../utils/states");
var entityDescriptor_1 = require("../entities/entityDescriptor");
var vehicles_1 = require("./../utils/vehicles");
var dates_1 = require("../utils/dates");
var status_1 = require("./status");
var DESCRIPTOR = {
    fields: [
        fields.buildField("id", "text"),
        fields.buildField("firstname", "text", "required|alpha_dash|min:2|max:25", "", true),
        fields.buildField("lastname", "text", "required|alpha_dash|min:2|max:25", "", true),
        fields.buildField("email", "text", "required|email|min:5|max:64", "", true),
        fields.buildField("password", "text", "required|min:8|max:64", null, true),
        fields.buildReferenceFieldWithSource("status", "status", status_1.SOURCE, "required", status_1.STATUS.REGISTERED, true),
        fields.buildField("phone", "text", "required|numeric|digits:10", "", true),
        fields.buildField("streetnumber", "text", "required|min:1|max:15", "", false, "street number"),
        fields.buildField("streetname", "text", "required|min:1|max:35", "", false, "street name"),
        fields.buildField("suburb", "text", "required|min:2|max:35", "", false),
        fields.buildReferenceFieldWithSource("state", "state", STATES_MODULE.STATE_SOURCE, "required", null, false),
        fields.buildField("postcode", "text", "required|numeric|digits:4", "", false),
        fields.buildReferenceField("vehicleYear", "year", "required", null, false, "vehicle year"),
        fields.buildField("vehicleMake", "text", "required|min:1|max:25", "", false, "vehicle make"),
        fields.buildField("vehicleModel", "text", "required|min:1|max:25", "", false, "vehicle model"),
        fields.buildReferenceFieldWithSource("vehicleType", "vehicleType", vehicles_1.VEHICLES_SOURCE, "required", null, false, "vehicle type"),
        fields.buildField("questionPhoneType", "text", "required|accepted", null, false, "question"),
        fields.buildField("questionLiftAndCarry", "text", "required|accepted", null, false, "question"),
        fields.buildField("questionDirectDebit", "text", "required|accepted", null, false, "question"),
        fields.buildField("accountName", "text", "required|min:2|max:35", "", false, "account name"),
        fields.buildField("bsb", "text", "required", "", false, "BSB"),
        fields.buildField("account", "text", "required", "", false),
        fields.buildReferenceField("dobDay", "day", "required", null, false, "day"),
        fields.buildReferenceFieldWithSource("dobMonth", "month", dates_1.MONTHS_SOURCE, "required", null, false, "month"),
        fields.buildReferenceField("dobYear", "year", "required", null, false, "year"),
        fields.buildField("drivingLicencePhoto", "attachment", "required|attachment", null, false, "driving licence photo"),
        fields.buildField("publicLiabilityInsurancePolicyNumber", "text", "max:20", ""),
        fields.buildReferenceField("publicLiabilityInsuranceExpiryDay", "day", "", null, false, "day"),
        fields.buildReferenceFieldWithSource("publicLiabilityInsuranceExpiryMonth", "month", dates_1.MONTHS_SOURCE, "", null, false, "month"),
        fields.buildReferenceField("publicLiabilityInsuranceExpiryYear", "year", "", null, false, "year"),
        fields.buildField("publicLiabilityInsurancePhoto", "attachment", "attachment", null, false, "public liability insurance photo"),
        fields.buildField("marineGoodsTransitInsurancePolicyNumber", "text", "max:20", ""),
        fields.buildReferenceField("marineGoodsTransitInsuranceExpiryDay", "day", "", null, false, "day"),
        fields.buildReferenceFieldWithSource("marineGoodsTransitInsuranceExpiryMonth", "month", dates_1.MONTHS_SOURCE, "", null, false, "month"),
        fields.buildReferenceField("marineGoodsTransitInsuranceExpiryYear", "year", "", null, false, "year"),
        fields.buildField("marineGoodsTransitInsurancePhoto", "attachment", "attachment", null, false, "public liability insurance photo"),
        fields.buildField("terms", "text", "accepted", false),
        fields.buildField("registeredAt", "text", "", false),
        fields.buildField("notification", "boolean", "", false),
        fields.buildField("insured", "boolean", "", false),
        fields.buildField("devices", "object", "", false)
    ],
    methods: {
        getStatus: function () {
            var _this = this;
            return _.find(status_1.STATUSES, function (status) { return status.value === _this.status; }).text;
        }
    }
};
exports.HELPER_DESCRIPTOR = new entityDescriptor_1.EntityDescriptor(DESCRIPTOR);
