import * as _ from "lodash";
import * as moment from "moment";
import {TYPE, TYPES} from "./types";
import {STATUSES} from "./status";
import {PAYMENT_STATUSES} from "./paymentStatus";

export const DATE_FORMAT = "DD MMM YYYY";

export interface JobDistance {
  text: string;
  value: number;
}

export interface JobDuration {
  text: string;
  value: number;
}

export interface JobItem {
  name: string;
  description: string;
}

export interface JobCoordinates {
  lat: number;
  lng: number;
}

export interface JobLocation {
  address: string;
  coordinates: JobCoordinates;
  placeId: string;
  stairs: number;
  lift: boolean;
  info: string;
}

export class BaseJob {
  public id: string;
  public firstname: string;
  public lastname: string;
  public phone: string;
  public email: string;
  public from: JobLocation;
  public to: JobLocation;
  public items: JobItem[];
  public startDate: number;
  public endDate: number;
  public pickUpTime: number;
  public jobType: number;
  public price: number;
  public discountPrice: number;
  public deliveryPrice: number;
  public distance: JobDistance;
  public duration: JobDuration;
  public confirmed: boolean;
  public assistInfo: boolean;
  public insuranceRequired: boolean;
  public createdAt: number;
  public confirmedAt: number;
  public updatedAt: number;
  public updatedBy: number;
  public status: number;
  public paymentStatus: number;
  public cancelReason: string;
  public offset: number;
  public helperId: string;
  public partnerId: string;
  public partner: object;

  public static parseJobs(jobs: any[]): BaseJob[] {
    return jobs.map(job => new BaseJob(job));
  }

  constructor(job: any) {
    this.id = job.id;
    this.firstname = job.firstname;
    this.lastname = job.lastname;
    this.phone = job.phone;
    this.email = job.email;
    this.from = job.from;
    this.to = job.to;
    this.items = job.items;
    this.startDate = job.startDate;
    this.endDate = job.endDate;
    this.pickUpTime = job.pickUpTime;
    this.jobType = job.jobType;
    this.price = job.price;
    this.discountPrice = job.discountPrice;
    this.deliveryPrice = job.deliveryPrice;
    this.distance = job.distance;
    this.duration = job.duration;
    this.assistInfo = job.assistInfo;
    this.insuranceRequired = job.insuranceRequired;
    this.createdAt = job.createdAt;
    this.confirmedAt = job.confirmedAt;
    this.updatedAt = job.updatedAt;
    this.updatedBy = job.updatedBy;
    this.status = job.status;
    this.paymentStatus = job.paymentStatus;
    this.cancelReason = job.cancelReason;
    this.offset = job.offset || 0;
    this.helperId = job.helperId;
    this.partnerId = job.partnerId;
    this.partner = job.partner;
  }

  public getStatus(): string {
    return _.find(STATUSES, status => status.value === this.status).text;
  }

  public getPaymentStatus(): string {
    return _.find(PAYMENT_STATUSES, status => status.value === this.status).text;
  }

  public getPaymentPrice(): string {
    return (this.discountPrice || this.price).toFixed(2);
  }

  public getDeliveryPrice(): string {
    return this.deliveryPrice.toFixed(2);
  }

  public getDisplayStartDate(format?: string): string {
    return moment(this.startDate).format(format || DATE_FORMAT);
  }

  public getDisplayEndDate(format?: string): string {
    return moment(this.endDate).format(format || DATE_FORMAT);
  }

  public getDisplayStartTime(format?: string): string {
    return moment(this.startDate).format(format || "HH");
  }

  public getDisplayEndTime(format?: string): string {
    return moment(this.endDate).format(format || "HH");
  }

  public getPickUpTime(format?: string): string {
    return moment(this.pickUpTime).format(format || "HH");
  }

  public getDisplayType(): string {
    return _.find(TYPES, status => status.id === this.jobType).label;
  }

  public getTimeToJob(): string {
    return moment().to(moment(this.startDate));
  }

  public isToday(): boolean {
    return moment().isSame(moment(this.startDate), "d");
  }

  public isTomorrow(): boolean {
    return moment().add(1, "day").isSame(moment(this.startDate), "d");
  }

  public isRubbishRemoval(): boolean {
    return this.jobType === TYPE.RUBBISH_REMOVAL;
  }
}
