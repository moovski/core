"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var moment = require("moment");
var types_1 = require("./types");
var status_1 = require("./status");
var paymentStatus_1 = require("./paymentStatus");
exports.DATE_FORMAT = "DD MMM YYYY";
var BaseJob = (function () {
    function BaseJob(job) {
        this.id = job.id;
        this.firstname = job.firstname;
        this.lastname = job.lastname;
        this.phone = job.phone;
        this.email = job.email;
        this.from = job.from;
        this.to = job.to;
        this.items = job.items;
        this.startDate = job.startDate;
        this.endDate = job.endDate;
        this.pickUpTime = job.pickUpTime;
        this.jobType = job.jobType;
        this.price = job.price;
        this.discountPrice = job.discountPrice;
        this.deliveryPrice = job.deliveryPrice;
        this.distance = job.distance;
        this.duration = job.duration;
        this.assistInfo = job.assistInfo;
        this.insuranceRequired = job.insuranceRequired;
        this.createdAt = job.createdAt;
        this.confirmedAt = job.confirmedAt;
        this.updatedAt = job.updatedAt;
        this.updatedBy = job.updatedBy;
        this.status = job.status;
        this.paymentStatus = job.paymentStatus;
        this.cancelReason = job.cancelReason;
        this.offset = job.offset || 0;
        this.helperId = job.helperId;
        this.partnerId = job.partnerId;
        this.partner = job.partner;
    }
    BaseJob.parseJobs = function (jobs) {
        return jobs.map(function (job) { return new BaseJob(job); });
    };
    BaseJob.prototype.getStatus = function () {
        var _this = this;
        return _.find(status_1.STATUSES, function (status) { return status.value === _this.status; }).text;
    };
    BaseJob.prototype.getPaymentStatus = function () {
        var _this = this;
        return _.find(paymentStatus_1.PAYMENT_STATUSES, function (status) { return status.value === _this.status; }).text;
    };
    BaseJob.prototype.getPaymentPrice = function () {
        return (this.discountPrice || this.price).toFixed(2);
    };
    BaseJob.prototype.getDeliveryPrice = function () {
        return this.deliveryPrice.toFixed(2);
    };
    BaseJob.prototype.getDisplayStartDate = function (format) {
        return moment(this.startDate).format(format || exports.DATE_FORMAT);
    };
    BaseJob.prototype.getDisplayEndDate = function (format) {
        return moment(this.endDate).format(format || exports.DATE_FORMAT);
    };
    BaseJob.prototype.getDisplayStartTime = function (format) {
        return moment(this.startDate).format(format || "HH");
    };
    BaseJob.prototype.getDisplayEndTime = function (format) {
        return moment(this.endDate).format(format || "HH");
    };
    BaseJob.prototype.getPickUpTime = function (format) {
        return moment(this.pickUpTime).format(format || "HH");
    };
    BaseJob.prototype.getDisplayType = function () {
        var _this = this;
        return _.find(types_1.TYPES, function (status) { return status.id === _this.jobType; }).label;
    };
    BaseJob.prototype.getTimeToJob = function () {
        return moment().to(moment(this.startDate));
    };
    BaseJob.prototype.isToday = function () {
        return moment().isSame(moment(this.startDate), "d");
    };
    BaseJob.prototype.isTomorrow = function () {
        return moment().add(1, "day").isSame(moment(this.startDate), "d");
    };
    BaseJob.prototype.isRubbishRemoval = function () {
        return this.jobType === types_1.TYPE.RUBBISH_REMOVAL;
    };
    return BaseJob;
}());
exports.BaseJob = BaseJob;
