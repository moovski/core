export declare class EntityDescriptor {
    fields: any[];
    constructor(data: any);
    getRules(): any;
    getBasicRules(): any;
    getFieldsFriendlyNames(): any;
}
