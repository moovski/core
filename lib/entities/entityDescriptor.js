"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var EntityDescriptor = (function () {
    function EntityDescriptor(data) {
        this.fields = data.fields;
        if (data.methods) {
            _.assign(this, data.methods);
        }
    }
    EntityDescriptor.prototype.getRules = function () {
        var rules = {};
        _.forEach(this.fields, function (field) {
            if (!field.basic && field.rules) {
                rules[field.name] = field.rules;
            }
        });
        return rules;
    };
    EntityDescriptor.prototype.getBasicRules = function () {
        var rules = {};
        _.forEach(this.fields, function (field) {
            if (field.basic) {
                rules[field.name] = field.rules;
            }
        });
        return rules;
    };
    EntityDescriptor.prototype.getFieldsFriendlyNames = function () {
        var names = {};
        _.forEach(this.fields, function (field) {
            if (field.friendlyName) {
                names[field.name] = field.friendlyName;
            }
        });
        return names;
    };
    return EntityDescriptor;
}());
exports.EntityDescriptor = EntityDescriptor;
