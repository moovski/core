import { ReferenceSource } from "../helper/status";
export declare const VEHICLES: {
    text: string;
    value: number;
}[];
export declare const VEHICLES_SOURCE: ReferenceSource;
