export declare const DATE_FORMAT = "DD MMM YYYY";
export interface JobDistance {
    text: string;
    value: number;
}
export interface JobDuration {
    text: string;
    value: number;
}
export interface JobItem {
    name: string;
    description: string;
}
export interface JobCoordinates {
    lat: number;
    lng: number;
}
export interface JobLocation {
    address: string;
    coordinates: JobCoordinates;
    placeId: string;
    stairs: number;
    lift: boolean;
    info: string;
}
export declare class BaseJob {
    id: string;
    firstname: string;
    lastname: string;
    phone: string;
    email: string;
    from: JobLocation;
    to: JobLocation;
    items: JobItem[];
    startDate: number;
    endDate: number;
    pickUpTime: number;
    jobType: number;
    price: number;
    discountPrice: number;
    deliveryPrice: number;
    distance: JobDistance;
    duration: JobDuration;
    confirmed: boolean;
    assistInfo: boolean;
    insuranceRequired: boolean;
    createdAt: number;
    confirmedAt: number;
    updatedAt: number;
    updatedBy: number;
    status: number;
    paymentStatus: number;
    cancelReason: string;
    offset: number;
    helperId: string;
    partnerId: string;
    partner: object;
    static parseJobs(jobs: any[]): BaseJob[];
    constructor(job: any);
    getStatus(): string;
    getPaymentStatus(): string;
    getPaymentPrice(): string;
    getDeliveryPrice(): string;
    getDisplayStartDate(format?: string): string;
    getDisplayEndDate(format?: string): string;
    getDisplayStartTime(format?: string): string;
    getDisplayEndTime(format?: string): string;
    getPickUpTime(format?: string): string;
    getDisplayType(): string;
    getTimeToJob(): string;
    isToday(): boolean;
    isTomorrow(): boolean;
    isRubbishRemoval(): boolean;
}
