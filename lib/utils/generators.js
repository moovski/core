"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.years = function (min) {
    if (min === void 0) { min = 1990; }
    var years = [];
    var current = new Date().getFullYear();
    while (current >= min) {
        years.push({
            text: current.toString(),
            value: current
        });
        current--;
    }
    return years;
};
exports.futureYears = function (max) {
    if (max === void 0) { max = 10; }
    var years = [];
    var current = new Date().getFullYear();
    var maxDate = new Date().getFullYear();
    while (current <= maxDate + max) {
        years.push({
            text: current.toString(),
            value: current
        });
        current++;
    }
    return years;
};
exports.days = function () {
    var days = [];
    var start = 1;
    while (start <= 31) {
        days.push({
            text: start.toString(),
            value: start
        });
        start++;
    }
    return days;
};
