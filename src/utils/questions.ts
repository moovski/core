export const QUESTIONS = {
  YESNO_OPTIONS: [
    { text: "Yes", value: "1" },
    { text: "No", value: "0" }
  ]
};
