"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.STATUS = {
    DRAFT: 1,
    ACTIVE: 2,
    DISABLED: 3
};
exports.STATUSES = [
    { value: exports.STATUS.DRAFT, text: "Draft" },
    { value: exports.STATUS.ACTIVE, text: "Active" },
    { value: exports.STATUS.DISABLED, text: "Disabled" }
];
exports.SOURCE = {
    getAll: function () {
        return exports.STATUSES;
    }
};
