export const TYPE = {
  FURNITURE: 1,
  SM_HOUSE_APP: 2,
  RUBBISH_REMOVAL: 3
};

export interface Type {
  id: number;
  label: string;
}

export let TYPES: Type[] = [
  { id: TYPE.FURNITURE, label: "Furniture pickup/delivery" },
  { id: TYPE.SM_HOUSE_APP, label: "Small house/apartment move" },
  { id: TYPE.RUBBISH_REMOVAL, label: "Rubbish and waste disposal" }
];
