import {days, years} from "./generators";
import {ReferenceSource} from "../helper/status";

export const MONTHS: String[] = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];

export interface IMapMonths { text: String; value: number; }

export interface ReferenceMonthSource {
  getAll (): IMapMonths[];
}

export const MONTHS_SOURCE: ReferenceMonthSource = {
  getAll(): IMapMonths[] {
    return DATE.MONTHS;
  }
};

export const DATE: any = {
  DAYS: days(),
  MONTHS: MONTHS.map((month, index) => {
    return { text: month, value: ++index };
  }),
  YEARS: years(1900)
};
