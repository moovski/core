export declare const STATUS: {
    QUOTE: number;
    BOOKED: number;
    CANCELLED: number;
    ACCEPTED: number;
    COMPLETED: number;
    REFUNDED: number;
    PAID: number;
};
export interface Status {
    value: number;
    text: string;
}
export declare let STATUSES: Status[];
