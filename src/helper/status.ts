export const STATUS = {
  REGISTERED: 1,
  COMPLETED: 2,
  VERIFIED: 3,
  SUSPENDED: 4,
  REJECTED: 5
};

export interface Status {
  value: number;
  text: string;
}

export let STATUSES: Status[] = [
  { value: STATUS.REGISTERED, text: "Registered" },
  { value: STATUS.COMPLETED, text: "Completed" },
  { value: STATUS.VERIFIED, text: "Verified" },
  { value: STATUS.SUSPENDED, text: "Suspended" },
  { value: STATUS.REJECTED, text: "Rejected" }
];

export interface ReferenceSource {
  getAll (): Status[];
}

export let SOURCE: ReferenceSource = {
  getAll (): Status[] {
    return STATUSES;
  }
};
