"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VEHICLES = [
    { text: "Ute", value: 1 },
    { text: "Trailer", value: 2 },
    { text: "Van", value: 3 },
    { text: "Truck", value: 4 }
];
exports.VEHICLES_SOURCE = {
    getAll: function () {
        return exports.VEHICLES;
    }
};
