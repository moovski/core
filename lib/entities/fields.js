"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var parserMap = {
    simpleParser: function (value) { return value; }
};
var serializeMap = {
    simpleReference: function (data) { return data && data.value ? data.value : data; },
    simpleSerializer: function (value) { return value; },
    integer: function (value) { return parseInt(value, 10); }
};
var referenceParserMap = {
    simpleReference: function (value) {
        return { text: value.toString(), value: parseInt(value, 10) };
    }
};
var Field = (function () {
    function Field(name, type, rules, defaultValue, basic, friendlyName) {
        this.name = name;
        this.type = type;
        this.rules = rules;
        this.defaultValue = defaultValue;
        this.basic = basic;
        this.friendlyName = friendlyName;
    }
    Field.prototype.parse = function (value) {
        var parser = parserMap[this.type] || parserMap.simpleParser;
        return parser(value) || this.defaultValue;
    };
    Field.prototype.serialize = function (value) {
        var serializer = serializeMap[this.type] || serializeMap.simpleSerializer;
        return serializer(value);
    };
    return Field;
}());
exports.Field = Field;
var ReferenceField = (function (_super) {
    __extends(ReferenceField, _super);
    function ReferenceField(name, referenceType, rules, defaultValue, basic, friendlyName) {
        var _this = _super.call(this, name, "reference", rules, defaultValue, basic, friendlyName) || this;
        _this.referenceType = referenceType;
        return _this;
    }
    ReferenceField.prototype.parse = function (value) {
        if (value === null || value === undefined) {
            return value;
        }
        var parser = referenceParserMap[this.referenceType] || referenceParserMap.simpleReference;
        return parser(value) || this.defaultValue;
    };
    ReferenceField.prototype.serialize = function (value) {
        var serializer = serializeMap[this.referenceType] || serializeMap.simpleReference;
        return serializer(value);
    };
    return ReferenceField;
}(Field));
exports.ReferenceField = ReferenceField;
var ReferenceSourceField = (function (_super) {
    __extends(ReferenceSourceField, _super);
    function ReferenceSourceField(name, referenceType, rules, defaultValue, basic, friendlyName, source) {
        var _this = _super.call(this, name, referenceType, rules, defaultValue, basic, friendlyName) || this;
        _this.referenceSource = source;
        return _this;
    }
    ReferenceSourceField.prototype.parse = function (value) {
        if (value === null || value === undefined) {
            return value;
        }
        if (!this.referenceSource) {
            throw new Error("reference source not found for type: " + this.referenceType);
        }
        return _.find(this.referenceSource.getAll(), function (state) { return state.value === value; });
    };
    return ReferenceSourceField;
}(ReferenceField));
exports.ReferenceSourceField = ReferenceSourceField;
exports.buildField = function (name, type, rules, defaultValue, basic, friendlyName) {
    return new Field(name, type, rules, defaultValue, basic, friendlyName);
};
exports.buildReferenceField = function (name, referenceType, rules, defaultValue, basic, friendlyName) {
    return new ReferenceField(name, referenceType, rules, defaultValue, basic, friendlyName);
};
exports.buildReferenceFieldWithSource = function (name, referenceType, source, rules, defaultValue, basic, friendlyName) {
    return new ReferenceSourceField(name, referenceType, rules, defaultValue, basic, friendlyName, source);
};
