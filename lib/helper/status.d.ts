export declare const STATUS: {
    REGISTERED: number;
    COMPLETED: number;
    VERIFIED: number;
    SUSPENDED: number;
    REJECTED: number;
};
export interface Status {
    value: number;
    text: string;
}
export declare let STATUSES: Status[];
export interface ReferenceSource {
    getAll(): Status[];
}
export declare let SOURCE: ReferenceSource;
