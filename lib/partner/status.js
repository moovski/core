"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.STATUS = {
    REGISTERED: 1,
    COMPLETED: 2,
    VERIFIED: 3,
    SUSPENDED: 4,
    REJECTED: 5
};
exports.STATUSES = [
    { value: exports.STATUS.REGISTERED, text: "Registered" },
    { value: exports.STATUS.COMPLETED, text: "Completed" },
    { value: exports.STATUS.VERIFIED, text: "Verified" },
    { value: exports.STATUS.SUSPENDED, text: "Suspended" },
    { value: exports.STATUS.REJECTED, text: "Rejected" }
];
exports.SOURCE = {
    getAll: function () {
        return exports.STATUSES;
    }
};
