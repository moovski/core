import {EntityDescriptor} from "./entityDescriptor";
import {Field} from "./fields";
import * as _ from "lodash";

export class Entity {
  public descriptor: EntityDescriptor;

  constructor (descriptor: EntityDescriptor, values: any = {}) {
    let fields = descriptor.fields;
    _.forOwn(fields, (field: Field) => {
      (<any>this)[field.name] = field.parse(values[field.name]) || field.defaultValue;
    });
    this.descriptor = descriptor;
  }

  serialize() {
    let output: any = {};
    this.descriptor.fields.forEach(field => {
      output[field.name] = field.serialize((<any>this)[field.name]);
    });
    return output;
  }

  getField (name: string) {
    return _.find(this.descriptor.fields, (field: Field) => field.name === name);
  }

  getRules (name: string) {
    return _.find(this.descriptor.fields, (field: Field) => field.name === name).rules;
  }
};

export const createEntity = (descriptor: EntityDescriptor, data: any) => {
  return new Entity(descriptor, data);
};
