import * as _ from "lodash";

const parserMap: any = {
  simpleParser: (value: any) => value
};

const serializeMap: any = {
  simpleReference: (data: any) => data && data.value ? data.value : data,
  simpleSerializer: (value: any) => value,
  integer: (value: any) => parseInt(value, 10)
};

const referenceParserMap: any = {
  simpleReference: (value: any) => {
    return { text: value.toString(), value: parseInt(value, 10) };
  }
};

export class Field {
  public name: string;
  public type: string;
  public rules: string;
  public defaultValue: any;
  public basic: boolean;
  public friendlyName: string;

  constructor(name: string, type: string, rules: string, defaultValue: any, basic: boolean, friendlyName: any) {
    this.name = name;
    this.type = type;
    this.rules = rules;
    this.defaultValue = defaultValue;
    this.basic = basic;
    this.friendlyName = friendlyName;
  }

  parse(value: any) {
    let parser = parserMap[this.type] || parserMap.simpleParser;
    return parser(value) || this.defaultValue;
  }

  serialize(value: any) {
    let serializer = serializeMap[this.type] || serializeMap.simpleSerializer;
    return serializer(value);
  }
}

export class ReferenceField extends Field {
  public referenceType: string;

  constructor(name: string, referenceType: string, rules: string, defaultValue: any, basic: boolean,
              friendlyName: any) {
    super(name, "reference", rules, defaultValue, basic, friendlyName);
    this.referenceType = referenceType;
  }

  parse(value: any) {
    if (value === null || value === undefined) {
      return value;
    }
    let parser = referenceParserMap[this.referenceType] || referenceParserMap.simpleReference;
    return parser(value) || this.defaultValue;
  }

  serialize(value: any) {
    let serializer = serializeMap[this.referenceType] || serializeMap.simpleReference;
    return serializer(value);
  }
}

export class ReferenceSourceField extends ReferenceField {
  public referenceSource: any;

  constructor(name: string, referenceType: string, rules: string, defaultValue: any, basic: boolean,
              friendlyName: any, source: any) {
    super(name, referenceType, rules, defaultValue, basic, friendlyName);
    this.referenceSource = source;
  }

  parse(value: any) {
    if (value === null || value === undefined) {
      return value;
    }
    if (!this.referenceSource) {
      throw new Error(`reference source not found for type: ${this.referenceType}`);
    }
    return _.find(this.referenceSource.getAll(), (state: any) => state.value === value);
  }
}

export const buildField = (name: string, type: string, rules?: string,
                           defaultValue?: any, basic?: boolean, friendlyName?: any) => {
  return new Field(name, type, rules, defaultValue, basic, friendlyName);
};

export const buildReferenceField = (name: string, referenceType: string, rules?: string,
                                    defaultValue?: any, basic?: boolean, friendlyName?: any) => {
  return new ReferenceField(name, referenceType, rules, defaultValue, basic, friendlyName);
};

export const buildReferenceFieldWithSource = (name: string, referenceType: string, source: any, rules?: string,
                                    defaultValue?: any, basic?: boolean, friendlyName?: any) => {
  return new ReferenceSourceField(name, referenceType, rules, defaultValue, basic, friendlyName, source);
};
