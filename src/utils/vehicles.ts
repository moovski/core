import {ReferenceSource} from "../helper/status";

export const VEHICLES = [
  { text: "Ute", value: 1 },
  { text: "Trailer", value: 2 },
  { text: "Van", value: 3 },
  { text: "Truck", value: 4 }
];

export const VEHICLES_SOURCE: ReferenceSource = {
  getAll() {
    return VEHICLES;
  }
};
